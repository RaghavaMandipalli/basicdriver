# This specifies the driver module
obj-m := hello.o

# Directory where basic driver module buildup takes place
KDIR ?= /lib/modules/`uname -r`/build

# This macro specified present working directory
PWD := `pwd`

# Building code for above module
default:
	$(MAKE) -C $(KDIR) M=$(PWD) modules

# Cleaning code of above module
clean:
	$(MAKE) -C $(KDIR) M=$(PWD) clean
