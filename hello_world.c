#include <linux/init.h>
#include <linux/module.h>

MODULE_LICENSE("Dual BSD/GPL");

/* Driver initialization module */

static int hello_init(void)
{
	printk(KERN_ALERT "Hello World\n");

	return 0;
}

/* Driver cleanup module */

static void hello_exit(void)
{
	printk(KERN_ALERT "Good bye, Cruel world\n");
	
	return;
}

/* Special macro which triggers the driver initialization fucntion */
module_init(hello_init);

/* Special macro which triggers the driver cleanup function */
module_exit(hello_exit);
